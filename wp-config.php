<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'gatsby' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '#gr VUf|X!]hVD8F}7R M`LNp0?{p)a;OO6oOZIq%+(MF{B^Bq!Fn$KSC.suYUX(' );
define( 'SECURE_AUTH_KEY',  'Ar2UMRJ~7=BGP:2`nqc(#S3_AZ`i%Qwhn?kKQc3u%Xa?iaxYq@a/Zu0;4*}-D[C=' );
define( 'LOGGED_IN_KEY',    'H|_h56BG^V/ds%xCG~~B,_OfyBW=)23da)Bahc=s[W(Ni[]uuhveLrp5?LR~bhVg' );
define( 'NONCE_KEY',        'P`OubZ9:>ST-pd[dRQJvh;*X,8nW-R]5DSh]`k-mG`Km9`$A@T+ZJHvn_VEjD%E1' );
define( 'AUTH_SALT',        'oL4wn3-{qa+}cZqNJ2bmz([4V.~vt/UNV4eTvig~.=u_5/b=0Ftzw&LJ%F:_.5{|' );
define( 'SECURE_AUTH_SALT', '3>^jGOuxtFXlx|O)V3~6t9eEDoFU{ns9t(*UIUqfO4EO+^ib;O] )t{@qOIdb/9z' );
define( 'LOGGED_IN_SALT',   'x;b4F?cX%V{y+}dbcj@}#DaM-{cN^&/DLhEHs5<~RjZ!aBAu1-4D9bp]~]g:0J)@' );
define( 'NONCE_SALT',       ']aNN4A-ITqL,*Lz5@MLov-k`.h8uWVd?Dg.vv8v[8]>WA3qK&+Uvx%?gBv7Fg<Rx' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );
define('FS_METHOD','direct');
/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
