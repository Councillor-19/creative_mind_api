<?php
add_theme_support( 'custom-logo' );
add_theme_support( 'menus' );
add_theme_support( 'post-thumbnails' );

function register_custom_post_type() {
	$args = array(
		'label'             => __( 'Services' ),
		'description'       => __( 'Service Description' ),
		'supports'          => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
		'menu_icon'         => 'dashicons-admin-generic',
		'public'            => true,
		'show_in_admin_bar' => true,
		'show_in_rest'      => true,
	);
	register_post_type( 'service', $args );
}

add_action( 'init', 'register_custom_post_type' );

/*add_filter( 'rest_menus_format_menu_item', 'remove_site_url' );

function remove_site_url( $menu_item ) {
	$menu_item['url'] = str_replace( home_url(), '', $menu_item['url'] );

	return $menu_item;
}*/

// Remove block editor for front page
/*add_filter( 'use_block_editor_for_post', function ( $enabled ) {
	return get_the_ID() != get_option( 'page_on_front' );
} );*/

function add_menu() {
	register_nav_menu('main-main', 'Main Menu');
}
add_action('after_setup_theme', 'add_menu');